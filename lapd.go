package main

import (
	"bytes"
	"crypto/ed25519"
	"crypto/hmac"
	"crypto/sha512"
	"crypto/subtle"
	"encoding/binary"
	"log"
	"net"
	"time"

	"github.com/google/uuid"

	"golang.org/x/crypto/chacha20poly1305"
	"golang.org/x/crypto/curve25519"
	"golang.org/x/crypto/hkdf"
)

type Request struct {
	Version          uint8
	Size             uint16
	ClientTimeRaw    [5]byte
	ClientBaseId     uuid.UUID
	ClientAddOnId    uuid.UUID
	SKUId            uuid.UUID
	CurrentLicenseId uuid.UUID
	Reserved         [16]byte
	ClientSeed       [14]byte // variable for each implementation
}

type Response struct {
	Version       uint8
	Size          uint16
	ServerTimeRaw [5]byte
	ClientId      uuid.UUID
	SKUId         uuid.UUID
	LicenseId     uuid.UUID
	ServerData    [14]byte // Encrypted "Hello, World!" for this example
	// ServerData must not be larger than ClientSeed
}

const (
	RequestHeaderSize  = 56
	ResponseHeaderSize = 56
	ProtocolVersion    = 2
)

var KeyExchangeSecretKey []byte
var KeyExchangePublicKey []byte
var SignatureSecretKey []byte
var SignaturePublicKey []byte
var ClientSeedMACKey []byte

var KnownSKU uuid.UUID

func deriveKeys(shared, theirpk []byte) ([]byte, []byte) {
	// We emulate hkdf.Extract here to avoid allocating a buffer for the
	// multi-part secret.
	salt := make([]byte, 64)
	h := hmac.New(sha512.New, salt)
	h.Write(theirpk)
	h.Write(KeyExchangePublicKey)
	h.Write(SignaturePublicKey)
	h.Write(shared)
	prk := h.Sum(nil)
	r := hkdf.Expand(sha512.New, prk, []byte("56065c4d-d2e0-4ba9-bf9f-76f9159e2987-LAP-V02"))
	clisk := make([]byte, 32)
	srvsk := make([]byte, 32)
	// The entropy limit is far above what we need; we can ignore errors in
	// r.Read() safely
	r.Read(clisk[:])
	r.Read(srvsk[:])

	return clisk, srvsk
}

func isValidUUID(id uuid.UUID) bool {
	// TODO check time values
	if id.Variant() != uuid.RFC4122 {
		log.Printf("bad UUID variant: %v (expected %v)", id.Variant(), uuid.RFC4122)
		return false
	}
	if id.Version() == 0 || id.Version() > 5 {
		// Future compatibility note: UUIDs v6-v8 are in development.
		// See draft-peabody-dispatch-new-uuid-format
		log.Printf("bad UUID version: %v (expected 1-4)", id.Version())
		return false
	}
	return true
}

func clientSeedHasValidMAC(clientSeed []byte) bool {
	h := hmac.New(sha512.New, ClientSeedMACKey)
	h.Write(clientSeed[11:])
	d := h.Sum(nil)

	return (subtle.ConstantTimeCompare(d[:11], clientSeed[:11]) == 1)
}

type RequestValidityStatus int

const (
	RequestValid RequestValidityStatus = iota
	RequestUnlikelyMalicious
	RequestLikelyMalicious
	RequestObviouslyMalicious
)

// This function doesn't need to be constant-time because the checking procedure
// is presumed to be public since it's documented in § 3.4
func (req *Request) getValidityStatus() RequestValidityStatus {
	if req.Version != ProtocolVersion {
		log.Printf("invalid version: %v", req.Version)
		return RequestUnlikelyMalicious // client too new
	}
	if req.Size <= 88 { // implicitly checked in the binary.Read though
		log.Printf("invalid size: %v", req.Size)
		return RequestUnlikelyMalicious // client too new?
	}
	if !isValidUUID(req.ClientBaseId) {
		log.Printf("client ID is not a valid UUID: %v", req.ClientBaseId)
		// Maybe one of the new UUID standards got passed by the IETF
		return RequestLikelyMalicious
	}
	if !isValidUUID(req.SKUId) {
		log.Printf("SKU ID is not a valid UUID: %v", req.SKUId)
		// Maybe one of the new UUID standards got passed by the IETF
		return RequestLikelyMalicious
	}
	ts := int64(0)
	for i := len(req.ClientTimeRaw) - 1; i >= 0; i-- {
		ts <<= 8
		ts |= int64(req.ClientTimeRaw[i])
	}
	ct := time.Unix(ts, 0).UTC()
	now := time.Now().UTC()
	delta := now.Sub(ct)
	if delta < 0 { // abs()
		delta = -delta
	}
	if delta.Seconds() > 30 {
		log.Printf("client time difference too large (%v)", delta)
		// More likely just a bad local clock not synced with net
		return RequestUnlikelyMalicious
	}

	if req.SKUId != KnownSKU {
		log.Printf("request for unknown SKU %v", req.SKUId)
		// Either a bad client or we've misconfigured our deployments;
		// in either case we want to make noise.
		return RequestLikelyMalicious
	}

	// In a real deployment, you'd have different MAC keys per SKU
	// and the client would check its SKU<->key relation locally;
	// therefore, there's an *active* attacker modifying packets code
	// to try invalid keys and see what happens.
	if !clientSeedHasValidMAC(req.ClientSeed[:]) {
		log.Printf("client seed has invalid MAC")
		return RequestObviouslyMalicious
	}

	// You may also want to do something with CurrentLicenseId,
	// like update a database with a fresh check-in.

	// Clients are always licensed properly here;
	// in real life, you'd probably want to make sure there are no duplicate
	// activations (and put a hardware identifier in the client seed)
	// or something along those lines
	return RequestValid
}

func reply(conn *net.UDPConn, raddr *net.UDPAddr, buf []byte) {
	theirpk := buf[:curve25519.PointSize]
	shared, err := curve25519.X25519(KeyExchangeSecretKey, theirpk)
	if err != nil {
		log.Printf("shared == 0, malicious client %v", raddr)
		return
	}

	clisk, srvsk := deriveKeys(shared, theirpk)
	cipher, err := chacha20poly1305.New(clisk)
	if err != nil {
		log.Fatal(err)
	}

	ciphertextWithTag := buf[curve25519.PointSize:]
	nonce := make([]byte, chacha20poly1305.NonceSize) // all zero
	reqbuf, err := cipher.Open(nil, nonce, ciphertextWithTag, nil)
	if err != nil {
		log.Printf("error decrypting request: %v", err)
		return
	}

	var req Request
	err = binary.Read(bytes.NewReader(reqbuf), binary.LittleEndian, &req)
	if err != nil {
		log.Printf("cannot parse request: %v", err)
		return
	}
	log.Println(req)
	switch req.getValidityStatus() {
	case RequestValid:
		log.Printf("good request by %v", raddr)
	case RequestUnlikelyMalicious:
		log.Printf("bad but unlikely to be malicious request by %v", raddr)
		return
	case RequestLikelyMalicious:
		log.Printf("likely malicious request by %v", raddr)
		return
	case RequestObviouslyMalicious:
		log.Printf("OBVIOUSLY MALICIOUS REQUEST BY %v", raddr)
		return
	}

	var res Response
	res.Version = ProtocolVersion
	res.Size = uint16(ResponseHeaderSize + len(res.ServerData))
	ts := time.Now().UTC().Unix()
	res.ServerTimeRaw[0] = (byte)(ts & 0xff)
	res.ServerTimeRaw[1] = (byte)((ts >> 8) & 0xff)
	res.ServerTimeRaw[2] = (byte)((ts >> 16) & 0xff)
	res.ServerTimeRaw[3] = (byte)((ts >> 24) & 0xff)
	res.ServerTimeRaw[4] = (byte)((ts >> 32) & 0xff)
	if req.ClientAddOnId != uuid.Nil { // This SKU can be both addon or base
		res.ClientId = req.ClientAddOnId
	} else {
		res.ClientId = req.ClientBaseId
	}
	res.SKUId = req.SKUId
	res.LicenseId = uuid.Must(uuid.NewRandom())
	copy(res.ServerData[:], []byte("Hello, World!\n"))

	// XXX everything here is a disaster of redundant allocations
	binresbuf := new(bytes.Buffer)
	err = binary.Write(binresbuf, binary.LittleEndian, res)
	if err != nil {
		log.Fatal("cannot create response: %v", err)
	}
	cipher, err = chacha20poly1305.New(srvsk)
	if err != nil {
		log.Fatal(err)
	}
	resbin := cipher.Seal(nil, nonce, binresbuf.Bytes(), nil)

	// We can at least reuse buf to hold our response since it is of equal
	// size.
	// IMPORTANT: Always make sure the client sends at least as many bytes
	// as your response will hold.
	// Otherwise, the server is vulnerable to a network amplification
	// attack where the client has to spend less bandwidth than the server.
	buffer := bytes.NewBuffer(buf[:0])
	buffer.Write(ed25519.Sign(SignatureSecretKey, resbin))
	buffer.Write(resbin)
	resbuf := buffer.Bytes()

	nw, err := conn.WriteToUDP(resbuf, raddr)
	if nw != len(resbuf) {
		log.Printf("didn't write full UDP packet!") // Should we retry?
		return
	}
	if err != nil {
		log.Printf("couldn't send for %v: %v", raddr, err)
		return
	}

	log.Printf("sent response %v", res)
}

func serve(conn *net.UDPConn, want_exit chan struct{}) {
	// size here depends on the size of your client seed
	buf := make([]byte, 32+16+RequestHeaderSize+46)

	for {
		nr, raddr, err := conn.ReadFromUDP(buf)
		if nr != len(buf) {
			log.Printf("request from %v with bad input length %v", raddr, nr)
		}
		if err != nil {
			log.Printf("request error %v", err)
		}

		cbuf := make([]byte, len(buf))
		copy(cbuf, buf)
		go reply(conn, raddr, cbuf)
	}

	want_exit <- struct{}{}
}

func main() {
	KeyExchangeSecretKey = []byte{0x4a, 0xc5, 0xc4, 0xe9, 0x22, 0xc7, 0xdf, 0xc4, 0x4d, 0x03, 0xc0, 0x7b, 0xdc, 0x82, 0x5c, 0x61, 0x65, 0xe9, 0xc5, 0xf2, 0x21, 0xab, 0x36, 0xd4, 0x05, 0xb7, 0x1e, 0xc6, 0xb8, 0x98, 0xc2, 0x48}
	KeyExchangePublicKey = []byte{0x6f, 0x87, 0x7b, 0x69, 0x0c, 0x19, 0xec, 0x4b, 0x51, 0xe8, 0xf9, 0xc9, 0x4f, 0xe9, 0x5e, 0x5b, 0x8f, 0xe5, 0x68, 0x78, 0xa2, 0x16, 0xe3, 0x0d, 0xd1, 0x20, 0x27, 0x92, 0xd1, 0xb4, 0x59, 0x30}
	SignatureSecretKey = []byte{0xe1, 0xb7, 0x53, 0x47, 0xef, 0x49, 0x70, 0x2c, 0x2f, 0xd3, 0x7a, 0x77, 0x26, 0x40, 0xaf, 0xc3, 0x44, 0x81, 0x0f, 0xed, 0x5d, 0x26, 0xd2, 0x76, 0x09, 0x94, 0x25, 0xd2, 0xe9, 0xd1, 0xf5, 0x8e, 0x91, 0x91, 0x3c, 0x85, 0x4f, 0x92, 0xa8, 0x36, 0xe2, 0xd7, 0x3e, 0x98, 0x71, 0x1e, 0x71, 0x2f, 0xcc, 0x41, 0x9f, 0x24, 0x0c, 0x50, 0xaf, 0x60, 0x50, 0xea, 0x6e, 0x2b, 0x3d, 0x32, 0x6a, 0xc9}
	SignaturePublicKey = []byte{0x91, 0x91, 0x3c, 0x85, 0x4f, 0x92, 0xa8, 0x36, 0xe2, 0xd7, 0x3e, 0x98, 0x71, 0x1e, 0x71, 0x2f, 0xcc, 0x41, 0x9f, 0x24, 0x0c, 0x50, 0xaf, 0x60, 0x50, 0xea, 0x6e, 0x2b, 0x3d, 0x32, 0x6a, 0xc9}
	ClientSeedMACKey = []byte{0x36, 0xd3, 0xe1, 0xb4, 0x06, 0xea, 0x32, 0x54, 0xa0, 0xa0, 0x01, 0x24, 0xd1, 0xfc, 0x39, 0xbd, 0x79, 0x22, 0x5e, 0x57, 0x10, 0x16, 0x48, 0xf4, 0x29, 0xc5, 0xfe, 0xd3, 0xa5, 0xe2, 0x41, 0xf1}
	KnownSKU = uuid.Must(uuid.Parse("afde6e72-3b0e-413d-8f4d-345fb84a7976"))
	laddr := &net.UDPAddr{
		IP:   net.ParseIP("127.0.0.1"),
		Port: 60101,
	}
	conn, err := net.ListenUDP("udp", laddr)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	want_exit := make(chan struct{})
	go serve(conn, want_exit)
	<-want_exit
}
